<?php
namespace console\controllers;

use common\models\Currency;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class CurrencyController
 * @package console\controllers
 */
class CurrencyController extends Controller
{
    /**
     * Url address of cbr, from where we can import currency data
     */
    const CBR_URL = 'http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * Default action. List of available commands
     */
    public function actionIndex()
    {
        echo 'yii currency/import' . PHP_EOL;
    }

    /**
     * Import currency data from external repository
     *
     * @param bool $forceDelete
     */
    public function actionImport($forceDelete = false)
    {
        try {
            Console::stdout('Start importing currency data...' . PHP_EOL);
            // Get currency data using curl
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::CBR_URL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);

            // Check data content
            if ($result !== false) {
                $xml = json_decode(json_encode((array) simplexml_load_string($result)), JSON_OBJECT_AS_ARRAY);
                if (isset($xml['Valute']) && is_array($xml['Valute'])) {
                    $total = count($xml['Valute']);

                    Console::startProgress(0, $total);

                    if ($forceDelete) {
                        Currency::deleteAll();
                    }

                    foreach($xml['Valute'] as $k => $valute) {
                        $currency = Currency::findOne(['id' => $valute['NumCode']]);

                        if (!$currency) {
                            $currency = new Currency();
                        }

                        $currency->id = $valute['NumCode'];
                        $currency->name = $valute['Name'];
                        $currency->rate = str_replace(',', '.', $valute['Value']);

                        if (!$currency->save()) {
                            Console::stderr(implode("\n", $currency->getFirstErrors()));
                        }

                        Console::updateProgress($k, $total);
                    }
                }
                Console::endProgress();
            } else {
                Console::stderr('No result. Please, try again later...');
            }
            Console::stdout('Currency data successfully imported');
            exit(0);
        } catch (\Exception $e) {
            Console::stderr($e->getMessage());
            exit(1);
        }
    }
}
