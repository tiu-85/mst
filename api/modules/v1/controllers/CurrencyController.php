<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;

/**
 * Class CurrencyController
 * @package api\modules\v1\controllers
 */
class CurrencyController extends ActiveController
{
    /**
     * @var string
     */
    public $modelClass = 'api\modules\v1\models\Currency';
}


