<?php
namespace api\modules\v1\models;

/**
 * Class Currency
 * @package api\modules\v1\models
 */
class Currency extends \common\models\Currency
{
    /**
     * Fields, which are will be returned
     *
     * @inheritDoc
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'name',
            'rate',
            'inserted_at' => function () {
                return date('Y-m-d H:i:s', $this->inserted_at);
            },
        ];
    }
}
