<?php
namespace api\modules\v1;

/**
 * Class Module
 * @package api\modules\v1
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'api\modules\v1\controllers';

    /**
     * Init API module
     */
    public function init()
    {
        parent::init();
    }
}
